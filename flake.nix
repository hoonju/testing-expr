{
  description = "hoonju";

  inputs = {

    haedosa.url = "github:haedosa/flakes";
    nixpkgs.follows = "haedosa/nixpkgs";
    flake-utils.follows = "haedosa/flake-utils";
    jupyter-overlay.url = "git+ssh://git@gitlab.com/haedosa/jupyter-overlay.git";


  };

  outputs =
    inputs@{ self, nixpkgs, flake-utils, ... }:
    {
      overlay = nixpkgs.lib.composeManyExtensions (
                  with inputs;
                  [
                    jupyter-overlay.overlay
                    (import ./overlay.nix)
                  ]);

    } // flake-utils.lib.eachDefaultSystem (system:

      let
        pkgs = import nixpkgs {
          inherit system;
          config = {};
          overlays = [ self.overlay ];
        };
      in rec {

        devShell = import ./develop.nix { inherit pkgs; };

        defaultPackage = packages.hoonju;
        packages = {
          hoonju = pkgs.hoonju;
          jupyter = pkgs.jupyterlab;
        };

        defaultApp = apps.hoonju;
        apps = let
          ghcEnv = pkgs.haskellPackages.ghcWithPackages (p: [ p.hoonju ]);
        in {
          jupyter = {
            type = "app";
            program = "${pkgs.jupyterlab}/bin/jupyter-lab";
          };
          ghci = {
            type = "app";
            program = "${ghcEnv}/bin/ghci";
          };
          hoonju = {
            type = "app";
            program = "${defaultPackage}/bin/hoonju";
          };
        };
      }
    );

}
