final: prev: with final; {

  haskellPackages = prev.haskellPackages.override (old: {
    overrides = lib.composeManyExtensions [
                  (old.overrides or (_: _: {}))
                  (haskell.lib.packageSourceOverrides { hoonju = ./.; })
                ];
  });

  hoonju = haskell.lib.justStaticExecutables haskellPackages.hoonju;

  ghcWithHoonju = haskellPackages.ghcWithPackages (p: [ p.hoonju ]);

  ghcWithHoonjuAndPackages = select :
    haskellPackages.ghcWithPackages (p: ([ p.hoonju ] ++ select p));


  jupyterlab = mkJupyterlab {
    haskellKernelName = "Hoonju";
    haskellPackages = p: with p;
      [ # add haskell pacakges if necessary
        hoonju
        hvega
        ihaskell-hvega
      ];
    pythonKernelName = "Hoonju";
    pythonPackages = p: with p;
      [ # add python pacakges if necessary
        scipy
        numpy
        tensorflow-bin
        matplotlib
        scikit-learn
        pandas
        lightgbm
      ];
  };

}
